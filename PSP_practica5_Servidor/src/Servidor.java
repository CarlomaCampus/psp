import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Servidor {

	
	
	void iniciar() {
		
		
		try {
			
			
			ServerSocket ss = new ServerSocket(5003);
			System.out.println("Esperando conexi�n...");
			Socket cliente = ss.accept();
			System.out.println("Conexi�n establecida con IP "+ cliente.getInetAddress() );
			DataInputStream entrada = new DataInputStream(cliente.getInputStream());
			DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());
			String mensaje = "";
			String lista = "";
			ArrayList<String> apuntados = new ArrayList<>();
			
			while(!"Salir".equals(mensaje)) {
				
				mensaje = entrada.readUTF();
				
				if("Apuntados".equals(mensaje)) {
					for(int i = 0; i < apuntados.size(); i++) {
						
						lista += apuntados.get(i)+", ";
					}
					salida.writeUTF(lista);
					lista = "";
				} else if (mensaje.length()>=8 &&"Nombre: ".equals(mensaje.substring(0, 8))){
					
					System.out.println("Se ha apuntado a una persona.");
					apuntados.add(mensaje.substring(8, mensaje.length()));
					
				} else {
					
					System.out.println("El cliente no ha entendido las instrucciones.");
					
				}
				
				
				
				
			}
			System.out.println("Desconectando servidor...");
			salida.close();
			entrada.close();
			ss.close();
			cliente.close();
			
			
		} catch (IOException e) {
			System.out.println(e);
		}
		
		
		
	}
	
	
	
	
	
	
}
