import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

public class Principal {
	
	static ArrayList<Usuario> participantes;
	public static void main(String[] args) {
		
		participantes = new ArrayList<>();
			
		
		try {
			ServerSocket ss = new ServerSocket(5003);
			
			while(true) {
			System.out.println("Esperando conexi�n...");
			Socket cliente = ss.accept();
			DataInputStream entrada = new DataInputStream(cliente.getInputStream());
			DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());
			System.out.println("Conexi�n establecida con IP "+ cliente.getInetAddress() );
			Usuario u = new Usuario(cliente, entrada, salida);
			new Thread(u).start();
			participantes.add(u);
			}
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		
	}
	

	
	
	
}
