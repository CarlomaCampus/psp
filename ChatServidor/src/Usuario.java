import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Usuario implements Runnable{
	
	
	Socket cliente;
	DataInputStream entrada;
	DataOutputStream salida;
	
	public Usuario(Socket cliente, DataInputStream entrada, DataOutputStream salida) {
		
		
	this.cliente = cliente;	
	this.entrada = entrada;
	this.salida = salida;
		
		
		
	}

	
	
	
	public void enviarmensaje(String mensaje) {
		
		try {
			salida.writeUTF(mensaje);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	@Override
	public void run() {
		try {
				String mensaje;
				
				while(true) {
					
					mensaje = entrada.readUTF();
					
					for(int i=0; i<Principal.participantes.size(); i++) {
						
						Principal.participantes.get(i).enviarmensaje(mensaje);
						
					}
					
					
				}
				
				
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
		
	}
	
	
}
